﻿using ClaimsManagementModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Sales_RegisterCustomer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Customer currCustomer = (Customer)Session["Customer"];

            if (currCustomer!=null)
            {
                tbFirstName.Text = currCustomer.FirstName;
                tbLastName.Text = currCustomer.LastName;
            }
        }
    }

    protected void NextButton_Click(object sender, EventArgs e)
    {
        Customer newCustomer = new Customer();
        newCustomer.FirstName=tbFirstName.Text;
        newCustomer.LastName = tbLastName.Text;

        Session["Customer"] = newCustomer;
        Server.Transfer("AssignPolicy.aspx");
    }
}