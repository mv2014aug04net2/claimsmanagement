﻿using System;
using System.Web.Security;

public partial class Admin_UsersAndRoles : System.Web.UI.Page
{   
    
    /// <summary>
    /// Populates the roles and users list when the page is first loaded.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
	    {
            foreach (MembershipUser user in Membership.GetAllUsers())
            {
                ddlUserList.Items.Add(user.UserName);
            }

            foreach (string role in Roles.GetAllRoles())
            {
                ddlRolesList.Items.Add(role);
            }
	    }
    }
    
    /// <summary>
    /// Displays the slescted users infomration
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSelectUser_Click(object sender, EventArgs e)
    {
        MembershipUser selectedUser;
        string username = ddlUserList.SelectedValue;

        hfSelectedUser.Value = username;    //Stores the username that we are editing

        if (!username.Equals("Users"))
	    {
            lblSlectedUserName.Text = username;
            selectedUser = Membership.GetUser(username);
            updateRolesList(username);
	    }
    }

    /// <summary>
    /// Assign the selected role to the selected user.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAddRole_Click(object sender, EventArgs e)
    {
        string username = hfSelectedUser.Value;

        if (ddlRolesList.SelectedIndex>0 && !username.Equals("Users"))
        {
            Roles.AddUserToRole(username, ddlRolesList.SelectedValue);
            updateRolesList(username);
        }
    }

    /// <summary>
    /// Removes the selected role from the selected user
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRemoveRole_Click(object sender, EventArgs e)
    {
        string username = hfSelectedUser.Value;

        if (lbAssignedRoles.SelectedValue!=null && !username.Equals("Users"))
        {
            Roles.RemoveUserFromRole(username, lbAssignedRoles.SelectedValue);
            updateRolesList(username);
        }

    }

    /// <summary>
    /// Displays the roles that are currently assigned to the spicified user.
    /// </summary>
    /// <param name="inUsername">The user whose roles are to be displayed.</param>
    private void updateRolesList(string inUsername)
    {
        lbAssignedRoles.Items.Clear();
        foreach (string role in Roles.GetRolesForUser(inUsername))
        {
            lbAssignedRoles.Items.Add(role);
        }
    }
}