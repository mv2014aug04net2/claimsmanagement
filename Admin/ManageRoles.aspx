﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ManageRoles.aspx.cs" Inherits="Admin_ManageRoles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <table>
        <tr>
            <td class="leftCol">
                <div id="RolesLabel">Current Roles</div>
                <br />
                <asp:ListBox ID="RolesList" runat="server"></asp:ListBox>
            </td>
            <td class="main">
                <asp:Label ID="NewRoleLabel" runat="server" Text="Enter a new role name"></asp:Label>
                <asp:TextBox ID="NewRoleTextBox" runat="server"></asp:TextBox>
                <asp:Button ID="NewRoleButton" runat="server" Text="Create New Role" 
                    onclick="NewRoleButton_Click" />
                <br /><br />
                <asp:Button ID="DeleteRoleButton" runat="server" Text="Delete Selected Role" 
                    Width="403px" onclick="DeleteRoleButton_Click" />
            </td>
        </tr>
    </table>
</asp:Content>

