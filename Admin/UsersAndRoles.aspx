﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="UsersAndRoles.aspx.cs" Inherits="Admin_UsersAndRoles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <table>
        <tr>
            <td>
                <asp:Label runat="server" Text="Please select a user:"></asp:Label>
            </td>
            <td><div class="SelectedUserInfo">
                <asp:Label ID="lblSlectedUserName" runat="server"></asp:Label>
            </div></td>
        </tr>
        <tr>
            <td><div class="UserSelect">
                <asp:DropDownList ID="ddlUserList" runat="server">
                    <asp:ListItem>Users</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="btnSelectUser" runat="server" Text="Select" 
                    onclick="btnSelectUser_Click" />
            </div></td>
            <td><div class="AssignedRoles">
                <asp:Label runat="server" Text="Assigned Roles"></asp:Label>
                <br />
                <asp:ListBox ID="lbAssignedRoles" runat="server" Width="196px"></asp:ListBox>
                <br />
                <asp:Button ID="btnRemoveRole" runat="server" Text="Remove Selected Role" 
                    onclick="btnRemoveRole_Click" />
            </div></td>
            <td><div class="RoleSelect" >
                <asp:HiddenField ID="hfSelectedUser" runat="server" Value="Users" />
                <asp:Label runat="server" Text="Select a role to add to the selected user:"></asp:Label>
                <br />
                <asp:DropDownList ID="ddlRolesList" runat="server">
                    <asp:ListItem>Roles</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="btnAddRole" runat="server" Text="Add Role" 
                    onclick="btnAddRole_Click" />
            </div></td>
        </tr>
    </table>

</asp:Content>

