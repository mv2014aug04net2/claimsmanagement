﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Admin_ManageRoles : System.Web.UI.Page
{
    /// <summary>
    /// Adds the available roles to the display list when the page loads.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            foreach (string role in Roles.GetAllRoles())
            {
                RolesList.Items.Add(role);
            }
        }
    }

    /// <summary>
    /// Removes the selected authorization role.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DeleteRoleButton_Click(object sender, EventArgs e)
    {
        try
        {
            Roles.DeleteRole(RolesList.SelectedValue);
            RolesList.Items.Remove(RolesList.SelectedItem);
        }
        catch (ArgumentException AE)
        {
            //no role was selected
        }
    }

    /// <summary>
    /// Creates the specified role.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void NewRoleButton_Click(object sender, EventArgs e)
    {
        Roles.CreateRole(NewRoleTextBox.Text);
        RolesList.Items.Add(NewRoleTextBox.Text);
    }
}