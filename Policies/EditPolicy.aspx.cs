﻿using ClaimsManagementModel;
using System;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public partial class Policies_EditPolicy : System.Web.UI.Page
{
    ClaimsManagementEntities database = new ClaimsManagementEntities();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Policy passedPolicy = (Policy)Session["Policy"];

            tbPolicyName.Text = passedPolicy.Name;
            taPolicyDescription.Value = passedPolicy.Description;
            
            foreach (Rule currRule in database.Policies.First(x=>x.ID==passedPolicy.ID).Rules)
            {
                lbAttachedRules.Items.Add(new ListItem(currRule.Name, currRule.ID.ToString()));
            }

            //Populates the list of exsisting rules
            foreach (Rule currRule in database.Rules.Where(x => x != null))
            {
                lbRuleList.Items.Add(new ListItem(currRule.Name, currRule.ID.ToString()));
            }
        }
    }

    /// <summary>
    /// Takes the user to the Edit Rule page to edit the selected rule.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditRule_Click(object sender, EventArgs e)
    {
        int selectedID = int.Parse(lbRuleList.SelectedValue);

        Session["Rule"] = database.Rules.First(x => x.ID == selectedID);
        database.Dispose();

        Session["LastURL"] = HttpContext.Current.Request.Url.AbsolutePath.ToString();
        Server.Transfer("~/Policies/EditRule.aspx");
    }

    protected void lbRuleList_SelectedIndexChanged(object sender, EventArgs e)
    {
        int selectedID = int.Parse(lbRuleList.SelectedValue);

        taRuleDescription.Value = database.Rules.First(x => x.ID == selectedID).Description;
    }
    
    /// <summary>
    /// Saves changes and returns user to last page.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSaveChanges_Click(object sender, EventArgs e)
    {
        Policy selectedPolicy = (Policy)Session["Policy"];

        selectedPolicy.Name = tbPolicyName.Text;
        selectedPolicy.Description = taPolicyDescription.Value;

        database.Attach(selectedPolicy);
        database.ObjectStateManager.ChangeObjectState(selectedPolicy, System.Data.EntityState.Modified);
        database.SaveChanges();
        database.Dispose();
        string lastPage = (string)Session["LastURL"];
        Session.Remove("LastURL");
        Server.Transfer(lastPage);
    }

    /// <summary>
    /// Discards changes and returns user to last page.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        database.Dispose();
        string lastPage = (string)Session["LastURL"];
        Session.Remove("LastURL");
        Server.Transfer(lastPage);
    }
}