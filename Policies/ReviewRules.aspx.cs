﻿using ClaimsManagementModel;
using System;
using System.Linq;
using System.Web;

public partial class Policies_ReviewRules : System.Web.UI.Page
{
    ClaimsManagementEntities database = new ClaimsManagementEntities();

    /// <summary>
    /// Adds rules to the display list when the page is first loaded.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            foreach (Rule currRule in database.Rules.Where(x => x != null))
            {
                lbActiveRules.Items.Add(currRule.Name);
            }
        }
    }

    /// <summary>
    /// Displays the description of a rule when it is selected.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbActiveRules_SelectedIndexChanged(object sender, EventArgs e)
    {   
        Rule selectedRule=getRuleFromName(lbActiveRules.SelectedValue);
        taDescription.Value=selectedRule.Description;
    }

    /// <summary>
    /// Takes the user to the edit rule page to edit the selected rule.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditRule_Click(object sender, EventArgs e)
    {
        Session["Rule"] = getRuleFromName(lbActiveRules.SelectedValue);
        database.Dispose();
        
        Session["LastURL"] = HttpContext.Current.Request.Url.AbsolutePath.ToString();
        Server.Transfer("~/Policies/EditRule.aspx");
    }

    /// <summary>
    /// Returns the Rule with the specified name
    /// </summary>
    /// <param name="name"></param>
    /// <returns>The rule with the specified name
    ///          Null if no rule with that name exsist</returns>
    private Rule getRuleFromName(string name)
    {
        return database.Rules.Where(x => x.Name.Equals(name)).First();
    }
}