﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="EditPolicy.aspx.cs" Inherits="Policies_EditPolicy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LoginContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table>
        <tr>
            <td>
                <asp:TextBox ID="tbPolicyName" runat="server"></asp:TextBox>
                <textarea id="taPolicyDescription" runat="server" rows="10" cols="40"></textarea>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
                    <asp:Label ID="Label1" runat="server" Text="Policy Rules:"></asp:Label>
                    <br />
                    <asp:ListBox ID="lbAttachedRules" runat="server"></asp:ListBox>
                    <asp:Button ID="btnAddRule" runat="server" Text="Add Rule" />
                    <asp:Button ID="btnRemoveRule" runat="server" Text="Remove Rule" />
                </ContentTemplate></asp:UpdatePanel>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                    <div class="ActiveRules" 
                        style="border-style: solid none none none; border-color: black; border-width: 1px; margin-top: 10px;">
                        <asp:Label ID="Label2" runat="server" Text="Active Rules:"></asp:Label>
                        <br />
                        <asp:ListBox ID="lbRuleList" runat="server" 
                            onselectedindexchanged="lbRuleList_SelectedIndexChanged" 
                            AutoPostBack="True"></asp:ListBox>
                        <textarea id="taRuleDescription" runat="server" rows="10" cols="40" readonly="readonly"></textarea>    
                    </div>
                </ContentTemplate></asp:UpdatePanel>
                <asp:Button ID="btnEditRule" runat="server" Text="Edit Rule" OnClick="btnEditRule_Click" />
            </td>
        </tr>
        <asp:Button ID="btnSaveChanges" runat="server" Text="Save Changes" 
            onclick="btnSaveChanges_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancel Changes" 
            onclick="btnCancel_Click" />
    </table>
</asp:Content>
