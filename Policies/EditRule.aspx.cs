﻿using ClaimsManagementModel;
using System;
using System.Linq;
using System.Web;

public partial class Policies_CreateRule : System.Web.UI.Page
{
    /// <summary>
    /// Loads the data from the rule to be edited into the appropriate fields
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Rule selectedRule = (Rule)(Session["Rule"]);
            tbRuleName.Text = selectedRule.Name;
            taDescription.Value = selectedRule.Description;
        }
    }
    
    /// <summary>
    /// Save the changes to the rule and return the user to the page they came from.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ClaimsManagementEntities database = new ClaimsManagementEntities();
        Rule selectedRule = (Rule)(Session["Rule"]);

        selectedRule.Name = tbRuleName.Text;
        selectedRule.Description = taDescription.Value;

        database.Attach(selectedRule);
        database.ObjectStateManager.ChangeObjectState(selectedRule, System.Data.EntityState.Modified);
        database.SaveChanges();
        database.Dispose();

        string lastPage = (string)Session["LastURL"];
        Session.Remove("LastURL");
        Server.Transfer(lastPage);
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        string lastPage = (string)Session["LastURL"];
        Session.Remove("LastURL");
        Server.Transfer(lastPage);
    }
}