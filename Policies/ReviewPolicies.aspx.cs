﻿using ClaimsManagementModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Policies_ReviewPolicies : System.Web.UI.Page
{
    private ClaimsManagementEntities database = new ClaimsManagementEntities();

    /// <summary>
    /// List the active policies in a list for the user to select from.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            foreach (Policy currPolicy in database.Policies)
            {
                lbPolicyList.Items.Add(new ListItem(currPolicy.Name, currPolicy.ID.ToString()));
            }
        }
    }

    /// <summary>
    /// Displays the information on the selected policy
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSelectPolicy_Click(object sender, EventArgs e)
    {
        int selectedID = int.Parse(lbPolicyList.SelectedValue);
        Policy selectedPolicy = database.Policies.First(x => x.ID==selectedID);

        lblPolicyName.Text = selectedPolicy.Name;
        hfPolicyID.Value = selectedPolicy.ID.ToString();
        taPolicyDescription.Value = selectedPolicy.Description;

        lbPolicyRules.Items.Clear();
        foreach (Rule currRule in selectedPolicy.Rules)
        {
            lbPolicyRules.Items.Add(new ListItem(currRule.Name, currRule.ID.ToString()));
        }
    }

    /// <summary>
    /// Displays the information on the selected rule.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbPolicyRules_SelectedIndexChanged(object sender, EventArgs e)
    {
        int selectedID = int.Parse(lbPolicyRules.SelectedValue);
        Rule selectedRule = database.Rules.First(x => x.ID == selectedID);

        lblRuleName.Text = selectedRule.Name;
        taRuleDescription.Value = selectedRule.Description;
    }

    /// <summary>
    /// Take the user to the Rule Edit page with the current Rule.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditRule_Click(object sender, EventArgs e)
    {
        int selectedID = int.Parse(lbPolicyRules.SelectedValue);
        Session["Rule"] = database.Rules.First(x => x.ID==selectedID);;
        
        database.Dispose();
        Session["LastURL"] = HttpContext.Current.Request.Url.AbsolutePath.ToString();
        Server.Transfer("~/Policies/EditRule.aspx");
    }

    /// <summary>
    /// Take the user to the Policy Edit page with the current policy.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditPolicy_Click(object sender, EventArgs e)
    {
        int policyID = int.Parse(hfPolicyID.Value);
        Session["Policy"] = database.Policies.First(x => x.ID == policyID);

        database.Dispose();
        Session["LastURL"] = HttpContext.Current.Request.Url.AbsolutePath.ToString();
        Server.Transfer("~/Policies/EditPolicy.aspx");
    }
}