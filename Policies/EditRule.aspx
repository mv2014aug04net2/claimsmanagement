﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="EditRule.aspx.cs" Inherits="Policies_CreateRule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <table>
        <tr>
            <asp:Label ID="lblCurrentRule" runat="server" Text='Rule Editing'></asp:Label>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" Text="Rule"></asp:Label>
                <br />
                <asp:TextBox ID="tbRuleName" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Label runat="server" Text="Description:"></asp:Label>
                <br />
                <textarea id="taDescription" runat="server" rows="5" cols="80"></textarea>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="Commit Changes" 
                    onclick="btnSave_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                    onclick="btnCancel_Click" />
            </td>
        </tr>
    </table>
</asp:Content>

