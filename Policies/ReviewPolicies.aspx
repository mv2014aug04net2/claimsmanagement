﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ReviewPolicies.aspx.cs" Inherits="Policies_ReviewPolicies" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LoginContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>

    <h3>Select a Policy</h3>
    <br />
    <asp:ListBox ID="lbPolicyList" runat="server"></asp:ListBox>
    <br />
    <asp:UpdatePanel ID="upPolicy" runat="server" UpdateMode="Conditional" 
        EnableViewState="False"><ContentTemplate>
        
        <asp:Button ID="btnSelectPolicy" runat="server" Text="Select Policy" 
            OnClick="btnSelectPolicy_Click"/>
        
            <br />
            <br />
        
        <asp:Label ID="lblPolicyName" runat="server"></asp:Label>
        <asp:HiddenField ID="hfPolicyID" runat="server" />
        <br />
        <textarea id="taPolicyDescription" runat="server" rows="10" cols="40" 
            readonly="readonly"></textarea>
        <br />
    </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSelectPolicy" />
        </Triggers>
    </asp:UpdatePanel>
    
    <asp:Button ID="btnEditPolicy" runat="server" Text="Edit Policy" 
        onclick="btnEditPolicy_Click" />

    <asp:UpdatePanel ID="upRule" runat="server"><ContentTemplate>
        <label id="Label1" runat="server">Attached rules</label>
        <br />
        <asp:ListBox ID="lbPolicyRules" runat="server" 
            onselectedindexchanged="lbPolicyRules_SelectedIndexChanged" 
            AutoPostBack="True"></asp:ListBox>
        <br />
        <asp:Label ID="lblRuleName" runat="server"></asp:Label>
        <br />
        <textarea id="taRuleDescription" runat="server" rows="10" cols="40" 
            readonly="readonly"></textarea>
        <br />
    </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lbPolicyRules" 
                EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>

    <asp:Button ID="btnEditRule" runat="server" Text="Edit Rule" 
        onclick="btnEditRule_Click" />
</asp:Content>

