﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ReviewRules.aspx.cs" Inherits="Policies_ReviewRules" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <table>
        <tr>
            <asp:UpdatePanel ID="upRuleDescription" runat="server"><ContentTemplate>
                <td>
                    <asp:Label runat="server" Text="Select a rule:"></asp:Label>
                    <br />
                    <asp:ListBox ID="lbActiveRules" runat="server" 
                        onselectedindexchanged="lbActiveRules_SelectedIndexChanged" AutoPostBack="True"></asp:ListBox>
                </td>
                <td>
                    <asp:Label runat="server" Text="Rule Description:"></asp:Label>
                    <br />
                    <textarea id="taDescription" runat="server" rows="5" cols="80" readonly="readonly"></textarea>
                </td>
            </ContentTemplate></asp:UpdatePanel>
            <td>
                <asp:Button ID="btnEditRule" runat="server" Text="Edit this rule" 
                    onclick="btnEditRule_Click" />
            </td>
        </tr>
    </table>

</asp:Content>

